var Jumper = cc.Sprite.extend({
    ctor: function( x, y ) {
        this._super();
        this.initWithFile( 'res/images/jumper.png' );
        this.setAnchorPoint( cc.p( 0.5, 0 ) );
        
        this.x = x;
        this.y = y;

        this.initProperties();

        this.updateSpritePosition();
    },
    initProperties: function() {
        this.vx = 0;
        this.vy = 0;

        this.moveLeft = false;
        this.moveRight = false;
        this.jump = false;

        this.ground = null;
        this.blocks = [];
    },

    updateSpritePosition: function() {
        var posX = Math.round( this.x );
        var posY = Math.round( this.y );
        this.setPosition( cc.p( posX, posY )) ;
    },

    getPlayerRect: function() {
        var spriteRect = this.getBoundingBoxToWorld();
        var spritePos = this.getPosition();

        var deltaX = this.x - spritePos.x;
        var deltaY = this.y - spritePos.y;

        var rectPos = cc.p( spriteRect.x + deltaX, spriteRect.y + deltaY );
        return cc.rect( rectPos.x, rectPos.y, spriteRect.width, spriteRect.height );
    },
    
    update: function() {
        var currentPositionRect = this.getPlayerRect();

        this.updateYMovement();
        this.updateXMovement();

        var newPositionRect = this.getPlayerRect();
        this.handleCollision( currentPositionRect, newPositionRect );

        this.updateSpritePosition();
    },

    updateXMovement: function() {
        if ( this.ground )
            this.onGroundXMovement();
        this.x += this.vx;
        this.borderCheckXMovement();
    },
    onGroundXMovement: function() {
        if ( !( this.moveLeft || this.moveRight ) )
            this.autoDeaccelerateX();
        else if ( this.moveRight )
            this.accelerateX( Jumper.DIRECTION.RIGHT );
        else
            this.accelerateX( Jumper.DIRECTION.LEFT );
    },
    borderCheckXMovement: function() {
        if ( this.x < 0 )
            this.x += screenWidth;
        else if ( this.x > screenWidth )
            this.x -= screenWidth;
    },

    updateYMovement: function() {
        if ( this.ground ) {
            this.vy = 0;
            if ( this.jump )
                this.jumping();
        } 
        else
            this.fallByGravity();
    },
    jumping: function() {
        this.vy = Jumper.JUMP_VY;
        this.y = this.ground.getTopY() + this.vy;
        this.ground = null;
    },
    fallByGravity: function() {
        this.vy += Jumper.GRAVITY;
        this.y += this.vy;
    },

    isSameDirection: function( dir ) {
        var condition1 = ( this.vx >=0 ) && ( dir >= 0 );
        var condition2 = ( this.vx <= 0 ) && ( dir <= 0 );
        return ( condition1 || condition2 );
    },

    accelerateX: function( dir ) {
        if ( this.isSameDirection( dir ) )
            this.accelerateXinSameDirection( dir );
        else
            this.accelerateXinOppositeDirection( dir );
    },
    accelerateXinSameDirection: function( dir ) {
        this.vx += dir * Jumper.ACCELERATION_X;
        if ( Math.abs( this.vx ) > Jumper.MAX_VX )
            this.vx = dir * Jumper.MAX_VX;
    },
    accelerateXinOppositeDirection: function( dir ) {
        if ( Math.abs( this.vx ) >= Jumper.BACK_ACCX )
            this.vx += dir * Jumper.BACK_ACCX;
        else
            this.vx = 0;
    },
    
    autoDeaccelerateX: function() {
        if ( Math.abs( this.vx ) < Jumper.ACCELERATION_X )
            this.vx = 0;
        else if ( this.vx > 0 )
            this.vx -= Jumper.ACCELERATION_X;
        else
            this.vx += Jumper.ACCELERATION_X;
    },

    handleCollision: function( oldRect, newRect ) {
        if ( this.ground ) {
            if ( !this.ground.onTop( newRect ) )
                this.ground = null;
        } 
        else if( this.vy <= 0 ) {
            var topBlock = this.findTopBlock( this.blocks, oldRect, newRect );
            if ( topBlock )
                this.standOnTheBlock( topBlock );
        }
    },
    standOnTheBlock: function( topBlock ) {
        this.ground = topBlock;
        this.y = topBlock.getTopY();
        this.vy = 0;
    },
    
    findTopBlock: function( blocks, oldRect, newRect ) {
        var topBlock = null;
        var topBlockY = -1;
        
        blocks.forEach( function( b ) {
            if ( b.hitTop( oldRect, newRect ) ) {
                if ( b.getTopY() > topBlockY ) {
                    topBlockY = b.getTopY();
                    topBlock = b;
                }
            }
        }, this );
        
        return topBlock;
    },
    
    handleKeyDown: function( e ) {
        if ( Jumper.KEYMAP[ e ] != undefined )
            this[ Jumper.KEYMAP[ e ] ] = true;
    },

    handleKeyUp: function( e ) {
        if ( Jumper.KEYMAP[ e ] != undefined )
            this[ Jumper.KEYMAP[ e ] ] = false;
    },

    setBlocks: function( blocks ) {
        this.blocks = blocks;
    }
});

Jumper.DIRECTION = {
    RIGHT: 1,
    LEFT: -1
};
Jumper.GRAVITY = -1;
Jumper.MAX_VX = 8;
Jumper.JUMP_VY = 20;
Jumper.BACK_ACCX = 0.5;
Jumper.ACCELERATION_X = 0.25;

Jumper.KEYMAP = {};
Jumper.KEYMAP[cc.KEY.left] = 'moveLeft';
Jumper.KEYMAP[cc.KEY.right] = 'moveRight';
Jumper.KEYMAP[cc.KEY.up] = 'jump';
        
